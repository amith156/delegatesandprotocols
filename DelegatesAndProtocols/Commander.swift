//
//  ViewController.swift
//  DelegatesAndProtocols
//
//  Created by Amith Kumar Narayan on 17/08/2019.
//  Copyright © 2019 Amith. All rights reserved.
//

import UIKit

protocol ordersDelegate {
    func folowingOrder(image : UIImage, title : String, color : UIColor)
}

class Commander: UIViewController {

    var CommanderDelegate : ordersDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func trainingAction(_ sender: Any) {
        CommanderDelegate.folowingOrder(image: UIImage(named: "training")!, title: "Perform Training!", color: .blue)
        performSegue(withIdentifier: "discriptionSegue", sender: nil)
    }
    
    @IBAction func guardAction(_ sender: Any) {
        CommanderDelegate.folowingOrder(image: UIImage(named: "guard")!, title: "Guard the country!", color: .orange)
        performSegue(withIdentifier: "discriptionSegue", sender: nil)
    }
    
    @IBAction func warAction(_ sender: Any) {
        CommanderDelegate.folowingOrder(image: UIImage(named: "War")!, title: "let's go to War!", color: .red)
        performSegue(withIdentifier: "discriptionSegue", sender: nil)
    }
    
}

